def build_deck
  suit = ["Diamonds", "Hearts", "Clubs", "Spades"]
  card_face = [2, 3, 4, 5, 6, 7, 8, 9, 10, "Jack", "Queen", "King", "Ace"]
  deck = []

  suit.each do |suit|
    card_face.each do |card_face|
      builder = {}
      builder[:name] = "#{card_face} of #{suit}"

      case card_face
      when "Jack", "Queen", "King"
        builder[:value] = 10
      when "Ace"
        #Sets Ace to false so it can be used as bool later
        builder[:value] = false
      else
        builder[:value] = card_face
      end

      deck << builder
    end
  end

  return deck.shuffle
end

def deal_card(deck, hand)
  hand << deck.pop
end

def get_sum(hand)
  sum = 0

  hand.each do |card|
    sum += card[:value] || 11
  end

  hand.each do |card|
    if card[:value] == 11
      #Set Ace to 1 to attempt to prevent a bust
      sum -= 10 if sum > 21
    end
  end

  return sum
end

def score_check(player, dealer, player_turn)
  if player_turn
    if get_sum(player) > 21
      puts "#{$player_name} has busted! Dealer wins!"
      puts "You lose all of your money!"

      game_over
    #End players turn if he gets 21
    elsif get_sum(player) == 21
      return false
    #Continue player turn if not >= 21
    else
      return true
    end
  end

  if !player_turn
    if get_sum(dealer) > 21
      puts "Dealer has busted! Player wins!"
    elsif get_sum(dealer) < get_sum(player)
      puts "#{$player_name} wins with a score of #{get_sum(player)} to #{get_sum(dealer)}!"
    elsif get_sum(dealer) > get_sum(player)
      puts "Dealer wins with a score of #{get_sum(dealer)} to #{get_sum(player)}!"
      puts "You lose all of your money!"
    elsif get_sum(dealer) == get_sum(player)
      puts "Tie game! #{get_sum(player)} to #{get_sum(dealer)}!"
    end
    game_over
  end
end


def print_hands(player, dealer, player_turn)
  system("cls")
  system("clear")

  puts "**********************"
  puts "* Dealer's Cards"
  if player_turn
  puts "* Face Down Card"
  puts "* #{dealer[1][:name]}"

  else
  dealer.each {|card| puts "* #{card[:name]}"}
  end

  puts "*"
  puts "* #{$player_name}'s Cards:"
  player.each {|card| puts "* #{card[:name]}"}

  if player_turn
    #To catch the fact that the value for Ace = nil
    puts "\n\nDealer shows #{dealer[1][:value] || 11}"
  else
  puts "\n\nDealer shows #{get_sum(dealer)}"
  end
  puts "#{$player_name} shows #{get_sum(player)}\n"
end

def play_game
  player_hand = []
  dealer_hand = []

  deck = build_deck

  #initial deal
  2.times do
    deal_card(deck, player_hand)
    deal_card(deck, dealer_hand)
  end

  #Skip to the end if someone got blackjack
  #player_turn is bool- true if neither player got blackjack, else false
  player_turn = (get_sum(player_hand) != 21 && get_sum(dealer_hand) != 21) || false
  print_hands(player_hand, dealer_hand, player_turn)
  player_turn || score_check(player_hand, dealer_hand,player_turn)

  #######################
  #    Player's Turn    #
  #######################
  while player_turn
    puts "Would you like to (h)it or (s)tay?"
    case gets.chomp.downcase
    when 'h', 'hit'
      deal_card(deck, player_hand)
      print_hands(player_hand, dealer_hand, player_turn)
      player_turn = score_check(player_hand, dealer_hand, player_turn)
    when 's', 'stay'
      player_turn = false
      print_hands(player_hand, dealer_hand, player_turn)
    else
      puts "Invalid Choice."
    end
  end

  #######################
  #   Computer's Turn   #
  #######################
  while get_sum(dealer_hand) < 17
    deal_card(deck, dealer_hand)
    print_hands(player_hand, dealer_hand, player_turn)
  end

  score_check(player_hand, dealer_hand, player_turn)
end

def game_over
  puts "\nWould you like to play again? (y/n)"
  case gets.chomp.downcase
  when 'y', 'yes'
    play_game
  when 'n', 'no'
    exit
  else
    puts "invalid choice"
    game_over
  end
end

print "Please enter your name: "
$player_name = gets.chomp.capitalize

play_game
